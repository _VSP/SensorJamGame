﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CharacterControl : MonoBehaviour {


    float timer;
    float totalTime;
    float enterTime;
    float exitTime;

    float quality;

    Transform startPosition;

    bool insideCollider = false;

    public float getQuality()
    {
        return quality;
    }

	// Use this for initialization
	void Start () {
        quality = 0;
	}
	
	// Update is called once per frame
	void Update () {
		

	}

    private void OnTriggerEnter(Collider other)
    {
        insideCollider = true;

        if(other.tag == "Line")
        {
            enterTime = Time.time;
        }
        
    }

    private void OnTriggerExit(Collider other)
    {
        if (other.tag == "Line")
        {
            exitTime = Time.time;
            timer = timer + 2 * (exitTime - enterTime);
            //Debug.Log(timer);
        }

    }

    public void ready()
    {
        totalTime = Time.time;
        quality = timer / totalTime;
        Debug.Log("Total time: " + totalTime);
        Debug.Log("player time: " + timer);
        Debug.Log("Quality: " + (quality)*100 + "%");
    }

}
