﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CheckPointScript : MonoBehaviour {

    GameObject Line;
    GameObject Player1;
    GameObject Player2;

    bool playersFinished = false;

    int TOTALCHECKPOINTS = 4;
    float GRACETIME = 2; //taikanumerooooooot

	// Use this for initialization
	void Start () {
        Line = GameObject.Find("Line");
        Player1 = GameObject.Find("Character1");
        Player2 = GameObject.Find("Character2");

        TOTALCHECKPOINTS = GameObject.Find("Lines").transform.childCount + 1;
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    private void OnTriggerEnter(Collider other)
    {
        //Line.GetComponent<CharacterControl>().ready();

        other.gameObject.GetComponent<PlayerScript>().addPlayerCheckpoints();

        if(other.gameObject.GetComponent<PlayerScript>().getPlayerCheckpoints() == TOTALCHECKPOINTS)
        {
            other.gameObject.GetComponent<CharacterControl>().ready();
            StartCoroutine(GraceTime(other.gameObject));
            
        }
    }

    IEnumerator GraceTime(GameObject player)
    {
        yield return new WaitForSeconds(GRACETIME);
        PlayerWin(player);
    }

    private void PlayerWin(GameObject player)
    {
        
        if(Player1.GetComponent<CharacterControl>().getQuality() > Player2.GetComponent<CharacterControl>().getQuality())
        {
            Debug.Log("Player1 Won!");
        }
        else
        {
            Debug.Log("Player2 Won!");
        }

    }
}
