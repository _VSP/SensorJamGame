﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerScript : MonoBehaviour {

    private int playerCheckpoints;


    public int getPlayerCheckpoints()
    {
        return playerCheckpoints;
    }

    public void setPlayerCheckpoints(int number)
    {
        playerCheckpoints = number;
    }

    public void addPlayerCheckpoints()
    {
        playerCheckpoints++;
    }



	// Use this for initialization
	void Start () {
		
	}


	
	// Update is called once per frame
	void Update () {
		
	}
}
