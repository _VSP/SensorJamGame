﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ClosesPointTest : MonoBehaviour {

    [SerializeField]
    GameObject target;
    float newPoint = 0, closestPoint = -1;
    Vector3 closestVector;
    BoxCollider closest;

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {

        GetComponent<LineRenderer>().SetPosition(0, transform.position);

        for(int i = 0; i < target.transform.childCount; i++){

            newPoint = Vector3.Distance(transform.position, target.transform.GetChild(i).transform.position);

            Debug.Log("Testing with child: " + target.transform.GetChild(i).name);

            if(closestPoint == -1 || newPoint < closestPoint){

                closestPoint = newPoint;
                closest = target.transform.GetChild(i).GetComponent<BoxCollider>();
                
                Debug.Log("HIT: " + closest.transform.position);
            }
        }

        Vector3 closestCoordinate = Physics.ClosestPoint(transform.position, closest, closest.transform.position, closest.transform.rotation);

        GetComponent<LineRenderer>().SetPosition(1, closestCoordinate);

        //GetComponent<LineRenderer>().SetPosition(1, closest.transform.position);

        closestPoint = -1;
	}
}
