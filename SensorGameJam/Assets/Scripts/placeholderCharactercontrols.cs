﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class placeholderCharactercontrols : MonoBehaviour {

	// Use this for initialization
	void Start () {
		
	}

    // Update is called once per frame
    void Update()
    {
        var x = Input.GetAxis("Horizontal") * Time.deltaTime * 10.0f;
        var z = Input.GetAxis("Vertical") * Time.deltaTime * 10.0f;

        transform.Rotate(0, 0, 0);
        transform.Translate(x, z, 0);
    }
}
