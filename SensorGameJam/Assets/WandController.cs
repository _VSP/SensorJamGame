﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WandController : MonoBehaviour {

    public float speed;
    public float targetRadius;

    RaycastHit hit;
    public Material reticuleMaterial;
    public GameObject reticule;
    GameObject instance;

	// Use this for initialization
	void Start () {
        reticule.GetComponent<Renderer>().material = reticuleMaterial;
        instance = Instantiate(reticule);
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    void FixedUpdate()
    {
        Vector3 up = transform.TransformDirection(Vector3.up);

        if (Physics.Raycast(transform.position, up, out hit))
        {
            Debug.DrawRay(transform.position, up*50, Color.green);
            if (hit.transform.tag != "Wand")
            {
                instance.transform.position = hit.point;
            }
        } else
        {
            Debug.DrawRay(transform.position, up*50, Color.red);
        }
    }
}
